
class Pet:
    def __init__(self, species=None, name=""):
        self.species = species
        if self.species not in ['Dog', 'Cat', 'Horse', 'Hamster']:  # Raise error if it is not of the following: 'dog','cat','horse','hamster'
            raise Exception('Error in the species attribute')
        self.name = name

    def __str__(self):
        if self.name !="":
            return f"Species of: {self.species}, named {self.name}"
        else:
            return f"Species of: {self.species},unnamed"


class Dog(Pet):
    def __init__(self, name="", chases="Cats"):
        self.name=name
        self.chases=chases
        super().__init__("Dog", self.name)

    def __str__(self):
        if self.name!="":
            return f"Species of: Dog, named {self.name}, chases {self.chases}"
        else:
            return f"Species of Dog, unnamed, chases {self.chases}"


class Cat(Pet):
    def __init__(self,name="",hates="Dogs"):
        self.name=name
        self.hates=hates
        super().__init__("Cat", self.name)

    def __str__(self):
        if self.name!="":
            return f"Species of: Cat, named {self.name}, hates {self.hates}"
        else:
            return f"Species of Cat, unnamed, hates {self.hates}"


class Main:  # To create objects and demonstrate their working
    dog = Dog('Rufus')
    dog_hates = Dog('Riley','Birds')
    cat = Cat('Cinnamon')
    cat_hates=Cat('Charlie','Loud noises')
    no_name_dog=Dog(chases='Mailmen')
    no_name_cat=Cat()
    no_error = Pet('Horse','Moony')
    print(dog,"\n",dog_hates,"\n",cat,"\n",cat_hates,"\n",no_name_cat,"\n",no_name_dog,"\n",no_error)

    call_error = Pet()






